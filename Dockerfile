#
# Build stage
#
FROM maven:3.6.0-jdk-8-slim AS build
COPY src /home/fakereports/src
COPY pom.xml /home/fakereports
RUN mvn -f /home/fakereports/pom.xml clean package -Dmaven.test.skip=true

#
# Package stage
#
FROM openjdk:8-jre-slim
COPY --from=build /home/fakereports/target /usr/local/lib/fakereports
EXPOSE 8081
ENTRYPOINT ["java","-jar","/usr/local/lib/fakereports/fakereports-0.1.jar"]