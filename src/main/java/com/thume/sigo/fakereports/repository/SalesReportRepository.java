package com.thume.sigo.fakereports.repository;

import fakereports.sigo.thume.Sales;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class SalesReportRepository {

    public List<Sales> getSalesReport(int month, int year) {
        List<Sales> list = new ArrayList<>();
        int length = 20;
        for (int i = 1; i <= length; i++) {
            Sales s = new Sales();
            s.setProductId(i);
            s.setSalesCount(randomSalesCount());
            list.add(s);
        }
        return list;
    }

    private int randomSalesCount() {
        return random(0, 500);
    }

    private int random(int min, int max) {
        return (int) (Math.random() * ((max - min) + 1)) + min;
    }

}
