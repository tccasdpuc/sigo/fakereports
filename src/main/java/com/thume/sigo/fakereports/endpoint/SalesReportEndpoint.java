package com.thume.sigo.fakereports.endpoint;

import fakereports.sigo.thume.GetSalesReportRequest;
import fakereports.sigo.thume.GetSalesReportResponse;
import fakereports.sigo.thume.Sales;
import com.thume.sigo.fakereports.repository.SalesReportRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
public class SalesReportEndpoint {

    public static final String NAMESPACE_URI = "thume.sigo.fakereports";

    private SalesReportRepository salesReportRepository;

    @Autowired
    public SalesReportEndpoint(SalesReportRepository salesReportRepository) {
        this.salesReportRepository = salesReportRepository;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getSalesReportRequest")
    @ResponsePayload
    public GetSalesReportResponse getCountry(@RequestPayload GetSalesReportRequest request) {
        GetSalesReportResponse response = new GetSalesReportResponse();
        List<Sales> salesReport = salesReportRepository.getSalesReport(request.getMonth(), request.getYear());
        response.getSales().addAll(salesReport);
        return response;
    }
}
