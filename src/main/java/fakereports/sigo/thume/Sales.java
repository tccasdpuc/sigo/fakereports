//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementação de Referência (JAXB) de Bind XML, v2.2.7 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modificações neste arquivo serão perdidas após a recompilação do esquema de origem. 
// Gerado em: 2020.09.29 às 10:00:05 PM BRT 
//


package fakereports.sigo.thume;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de sales complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="sales">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="productId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="salesCount" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "sales", propOrder = {
    "productId",
    "salesCount"
})
public class Sales {

    protected int productId;
    protected int salesCount;

    /**
     * Obtém o valor da propriedade productId.
     * 
     */
    public int getProductId() {
        return productId;
    }

    /**
     * Define o valor da propriedade productId.
     * 
     */
    public void setProductId(int value) {
        this.productId = value;
    }

    /**
     * Obtém o valor da propriedade salesCount.
     * 
     */
    public int getSalesCount() {
        return salesCount;
    }

    /**
     * Define o valor da propriedade salesCount.
     * 
     */
    public void setSalesCount(int value) {
        this.salesCount = value;
    }

}
